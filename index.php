<?php
class Person
{
    protected $_name='Ivan';
    public function hello()
    {
        echo "Hello. My name is ".$this->_name." \n";
        return $this;
    }
    public function age()
    {
        echo "I am 30.\n";
        return $this;
    }
}

$ivan= new Person();
$ivan->hello();
$ivan->age();
?>

